<?php /*a:3:{s:49:"G:\phpstudy_pro\WWW\tp\view\home\goods\index.html";i:1605169167;s:51:"G:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;s:54:"G:\phpstudy_pro\WWW\tp\view\home\common\resources.html";i:1603609812;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
<!--<link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.css" rel="stylesheet">-->
<!--<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css">-->
<link href="/static/bootstrap3.0.css" rel="stylesheet" type="text/css"/>
<script src="/static/bootstrap/js/bootstrap.js"></script>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a>
                    <cite>商品模型</cite>
                </a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5" method="get" action="">
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input class="layui-input" placeholder="开始日" name="start" id="start"></div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input class="layui-input" placeholder="截止日" name="end" id="end"></div>

                                <div class="layui-input-inline layui-show-xs-block">
                                    <input type="text" name="name" placeholder="请输入商品名称" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button class="layui-btn" lay-submit="" lay-filter="sreach">
                                        <i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-header">
                            <button class="layui-btn" onclick="xadmin.open('添加用户','/home/goods/adds',500,250)">
                                <i class="layui-icon"></i>添加</button></div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>商品名称</th>
                                        <th>价格</th>
                                        <th>创建时间</th>
                                        <th>操作</th></tr>
                                </thead>
                                <tbody>
                                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                    <tr>
                                        <td>
                                            <img src="/static/ico/id.png"/>
                                            <?php echo htmlentities($vo['id']); ?>
                                        </td>
                                        <td>
                                            <img src="/static/ico/goods_name.png"/>
                                            <?php echo htmlentities($vo['name']); ?>
                                        </td>
                                        <td>
                                            <img src="/static/ico/price.png"/>
                                            <?php echo htmlentities($vo['price']); ?>元
                                        </td>
                                        <td>
                                            <img src="/static/ico/time.png"/>
                                            <?php echo date('Y-m-d H:i:s',$vo['create_time']); ?>
                                        </td>
                                        <td class="td-manage">
                                            <a title="查看" onclick="xadmin.open('编辑','/home/goods/edits/id/'+<?php echo htmlentities($vo['id']); ?>,500,250)" href="javascript:;">
                                                <i class="layui-icon">&#xe63c;</i></a>
                                            <a title="删除" onclick="member_del(this,<?php echo htmlentities($vo['id']); ?>)" href="javascript:;">
                                                <i class="layui-icon">&#xe640;</i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </tbody>
                            </table>
                            <?php echo $list; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

<script>
    layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;
            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
                // ,value: new Date()
            });
            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
                // ,value: new Date(new Date().getTime() + 24*60*60*1000)
            });
        });
/*商品模型-删除*/
function member_del(obj, id) {
    layer.confirm('确认要删除吗？',
        function(index) {
            $.ajax({
                type:"post",
                url: "<?php echo url('home/goods/deletes'); ?>",
                data: {
                    id:id
                },
                success: function(data){
                    console.log(data);
                    toastr.error(data.msg);
                    if(data.code == 100){

                        layer.closeAll();
                        $(obj).parents("tr").remove();
                        setTimeout(function () {
                            location.reload();
                        },1000);
                    }
                }});
        });
}

</script>


</html>