<?php /*a:3:{s:51:"D:\phpstudy_pro\WWW\tp\view\home\systems\basic.html";i:1604886969;s:51:"D:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;s:54:"D:\phpstudy_pro\WWW\tp\view\home\common\resources.html";i:1603609812;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
<!--<link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.css" rel="stylesheet">-->
<!--<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css">-->
<link href="/static/bootstrap3.0.css" rel="stylesheet" type="text/css"/>
<script src="/static/bootstrap/js/bootstrap.js"></script>

<style>
    a{
        text-decoration:none;
    }
    a:hover{
        text-decoration:none;
    }
</style>
<body>
<div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a>
                    <cite>酒店设置</cite>
                </a>
            </span>
    <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
        <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
    </a>
</div>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                    <div class="layui-card-body ">
                    <form action="" method="post">
                    <table class="layui-table layui-form">

                        <tbody>
                        <tr>
                            <td>
                                <!--图片上传开始-->
                                <input type="file" id="photoFile" style="display: none;" onchange="upload()">

                                <img id="preview_photo" src="<?php echo htmlentities($list['logo']); ?>" width="100px" height="100px">
                                <a href="javascript:void(0)" onclick="uploadPhoto()" class="layui-btn layui-btn-sm layui-btn-warm ">选择图片</a>
                                <!--图片上传end-->
                            </td>
                            <button class="layui-btn" type="submit">
                                <i class="layui-icon"></i>保存基本设置
                            </button>
                        </tr>
                        <tr>
                            <td>
                                <div class="layui-form-item">
                                    <label for="username" class="layui-form-label">
                                        <span class="x-red">酒店名称</span>
                                    </label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="hotel_name" name="hotel_name" required="" lay-verify="required"
                                               autocomplete="off" class="layui-input" value="<?php echo htmlentities($list['hotel_name']); ?>">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="layui-form-item">
                                    <label for="username" class="layui-form-label">
                                        <span class="x-red">酒店电话</span>
                                    </label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="tel" name="tel" required="" lay-verify="required"
                                               autocomplete="off" class="layui-input" value="<?php echo htmlentities($list['tel']); ?>">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="layui-form-item">
                                    <label for="username" class="layui-form-label">
                                        <span class="x-red">酒店设施</span>
                                    </label>
                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <?php if(is_array($facilities) || $facilities instanceof \think\Collection || $facilities instanceof \think\Paginator): $i = 0; $__LIST__ = $facilities;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                                <input type="checkbox" name="facilities[]" value="<?php echo htmlentities($vo['name']); ?>" title="<?php echo htmlentities($vo['name']); ?>" <?php if(in_array($vo['name'],explode(",", $list['facilities']))): ?>checked <?php endif; ?>  >
                                            <?php endforeach; endif; else: echo "" ;endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="layui-form-item">
                                    <label for="username" class="layui-form-label">
                                        <span class="x-red">酒店地址</span>
                                    </label>
                                    <div class="layui-input-inline">
                                        <input type="text" id="address" name="address" required="" lay-verify="required"
                                               autocomplete="off" class="layui-input" value="<?php echo htmlentities($list['address']); ?>">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="layui-form-item">
                                    <label for="username" class="layui-form-label">
                                        <span class="x-red">酒店介绍</span>
                                    </label>

                                    <div class="layui-input-block">
                                        <textarea name="text" placeholder="酒店介绍" class="layui-textarea" id="text"><?php echo htmlentities($list['text']); ?></textarea>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </form>
                </div>

                <button type="button" class="layui-btn" onclick="uploadimg()">添加酒店演示图</button>
                <input type="file" id="imgFile" style="display: none;" onchange="hotel()">

                <div class="layui-carousel" id="test1">
                    <div carousel-item>
                        <?php if(is_array($hotel_img) || $hotel_img instanceof \think\Collection || $hotel_img instanceof \think\Paginator): $i = 0; $__LIST__ = $hotel_img;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$img): $mod = ($i % 2 );++$i;?>
                        <div>
                            <button type="button" class="layui-btn layui-btn-xs layui-btn-danger" onclick="remove_img(<?php echo htmlentities($img['id']); ?>)"
                                style="float: right;margin-bottom: -60px;position:relative;z-index:777;" >
                                移除图片
                            </button>
                            <img src="<?php echo htmlentities($img['src']); ?>" width="100%" >
                        </div>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
</body>

<script>
    layui.use(
            ['laydate', 'form']
    );

    layui.use('carousel', function(){
        var carousel = layui.carousel;
        //建造实例
        carousel.render({
            elem: '#test1'
            ,width: '100%' //设置容器宽度
            ,arrow: 'always' //始终显示箭头
            ,height:"800px"
            //,anim: 'updown' //切换动画方式
        });
    });

    function uploadPhoto() {
        $("#photoFile").click();
    }

    /**
     * 上传图片
     */
    function upload() {
        if ($("#photoFile").val() == '') {
            return;
        }
        var formData = new FormData();
        formData.append('photo', document.getElementById('photoFile').files[0]);
        $.ajax({
            url:"<?php echo url('home/systems/uploadPhoto'); ?>",
            type:"post",
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.type == "success") {
//                    $("#preview_photo").attr("src", data.filepath+data.filename);
                    $("#preview_photo").attr("src", data.filepath);
                    $("#productImg").val(data.filename);
                } else {
                    alert(data.msg);
                }
            },
            error:function(data) {
                alert("上传失败")
            }
        });
    }
/*-------上传酒店环境图*/
    function uploadimg() {
        $("#imgFile").click();
    }

    function hotel(){
        if ($("#imgFile").val() == '') {
            return;
        }

        var formData = new FormData();
        formData.append('img', document.getElementById('imgFile').files[0]);
        $.ajax({
            url:"<?php echo url('home/systems/hotel_img'); ?>",
            type:"post",
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.type == "success") {
                    location.reload();
                }
            },
            error:function(data) {
                alert("上传失败")
            }
        });
    }

    /*
    * 删除酒店场景图片
    * */
    function remove_img(id){
        $.ajax({
            type:"post",
            url: "<?php echo url('home/systems/remove_img'); ?>",
            data: {
                id:id
            },
            success: function(data){
                console.log(data);
                toastr.error(data.msg);
                if(data.code == 100){
                    setTimeout(function () {
                        location.reload();
                    },1000);
                }
            }});
    }

</script>


</html>