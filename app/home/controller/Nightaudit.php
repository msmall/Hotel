<?php
namespace app\home\controller;

use app\index\controller\Basics;
use think\facade\Db;


/*
 * 夜审设置
 *
 * */

class Nightaudit extends Basics
{
    /*
     * 夜审设置
     *
     * */
    public function index()
    {
        if(request()->isPost()){
   /*         $map = [
                ['building_id','=',session('building_id')],
                ['id','=','1']
            ];*/
            Db::table('home_nighttrial_system')->where('building_id',session('building_id'))->update(input('param.'));
        }
        $list =  Db::table('home_nighttrial_system')->where('building_id',session('building_id'))->find();
        return view('index',['list'=>$list]);
    }

    /*
     * 应到未到
     * */
    public function future(){
        $list =  Db::table('home_nighttrial_system')->where('building_id',session('building_id'))->find();
        $time = date('Y-m-d');
        $map = [
             ['building_id','=',session('building_id')],
             ['in_time','like',$time.'%']
         ];
        $room = Db::table('room')->where($map)->paginate(10);
        dump($room);
        return view();
    }

}
