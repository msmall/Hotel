<?php
namespace app\home\controller;

use app\index\controller\Basics;
use think\facade\Db;
use think\View;

/*
 * 房态管理
 * */
class Atrial extends Basics
{

    /*
     * 置空房间
     * */
    public function emptys(){
        if(request()->isAjax()){
            $data = [
                'status' => 1,
                'room_id' => 'no',
                'guest_name' => '',
                'activity_id' => '',
                'credentials' => '',
                'guest_sex' =>'',
                'guest_source' =>'',
                'guest_number' =>'',
                'move_duration' =>'',
                'move_time' => '',
                'in_time' => ''
            ];
            if( Db::name('room')->where('id',input('id'))->update($data)){
                $res = Db::name('room')->where('id',input('id'))->find();
                Db::table('hotel_move_ren')->where('room_id',$res['room_num'])->delete();
                return $this->return_json('操作成功','100');
            }else{
                return $this->return_json('操作失败','0');
            }
        }
    }

    /*
     * 故障报修
     * */
    public function report(){
        if(request()->isAjax()){
            $data = input('param.');
            $data['status'] = '4';
            if( Db::name('room')->update($data)){
                return $this->return_json('操作成功','100');
            }else{
                return $this->return_json('操作失败','0');
            }
        }
    }

    /*
     * 锁房
     * */
    public function lock_room(){
        if(request()->isAjax()){
            $data = input('param.');
            $data['status'] = '5';
            if( Db::name('room')->update($data)){
                return $this->return_json('操作成功','100');
            }else{
                return $this->return_json('操作失败','0');
            }
        }
    }

    /*
     * 退房业务
     * */
    public function out_room(){
        if(request()->isAjax()){
            $data = [
                'status' => 1,
                'room_id' => 'no',
                'guest_name' => '',
                'activity_id' => '',
                'credentials' => '',
                'guest_sex' =>'',
                'guest_source' =>'',
                'guest_number' =>'',
                'move_duration' =>'',
                'move_time' => '',
                'in_time' => ''
            ];
            $datas = input('param.');

            //添加退房记录
            if(isset($datas['deposit'])){
                $res =[
                    'room_rate'=>$datas['room_rate'],
                    'deposit'=>$datas['deposit'],
                    'reason'=>$datas['reason'],
                    'desc'=>$datas['desc'],
                    'room_id'=>$datas['id'],
                    'building_id'=>session('building_id'),
                    'operator'=> session('admin_id'),
                    'create_time'=>time()
                ];

                 Db::name('home_room_refund')->insert($res);

                //判断是不是所有房间退款
                if(isset($datas['all'])){
                    $data1 = Db::name('home_room_income')->where('parent_id',$datas['id'])->find();

                    if( Db::name('room')->where('id','IN',$data1['room_arr'])->update($data)){
                        //修改该房间下面的住户
                        Db::name('hotel_move_ren')->where('room_id','IN',$data1['num_arr'])->delete();
                        Db::name('income')->where('room_id','IN',$data1['room_arr'])->delete();
                        Db::name('consume')->where('room_id','IN',$data1['room_arr'])->update(['status'=>'0']);
                        Db::name('home_room_income')->where('parent_id','IN',$data1['room_arr'])->update(['status'=>'0']);
                        return $this->return_json('操作成功','100');
                    }else{
                        return $this->return_json('操作失败','0');
                    }
                }
            }
           //单个房间办理退房
            if( Db::name('room')->where('id',input('id'))->update($data)){
                //添加退房记录
                    $res =[
                        'room_rate'=>'0',
                        'deposit'=>'0',
                        'reason'=>'',
                        'desc'=>'',
                        'room_id'=>input('id'),
                        'building_id'=>session('building_id'),
                        'operator'=> session('admin_id'),
                        'create_time'=>time()
                    ];

                    Db::name('home_room_refund')->insert($res);
                //修改该房间下面的住户
                $list = Db::name('room')->where('id',input('id'))->find();
                Db::name('hotel_move_ren')->where('room_id',$list['room_num'])->delete();
                Db::name('income')->where('room_id',input('id'))->delete();
                Db::name('consume')->where('room_id',input('id'))->update(['status'=>'0']);
                Db::name('home_room_income')->where('parent_id',input('id'))->update(['status'=>'0']);
                return $this->return_json('操作成功','100');
            }else{
                return $this->return_json('操作失败','0');
            }
        }
        if(request()->isGet()){
            $id = input('id');
            $list = Db::name('home_room_income')->where(['parent_id'=>$id,'status'=>'1'])->find();
        }
        return \view('out_room',['list'=>$list,'id'=>$id]);
    }

    /*
     * 添加消费
     * */
    public function consume()
    {
        $list = $this->select_find('room',['id' => input('id')]);
        $data =  Db::table('consume')
            ->alias('a')
            ->field('a.*,b.room_num,d.number,d.name,d.price')
            ->join('room b','a.room_id = b.id')
            ->join('goodss d','a.goods_id = d.id')
            ->where(['a.room_id'=>input('id'),'a.status'=>'1'])
            ->paginate(10);
        return view('consume',['list' => $list,'data' => $data]);
    }

    /*
     * 选择添加商品
     * */
    public function goods()
    {
        if(request()->isAjax()){
            $data = input('param.');
            $data['create_time'] = time();
            $data['operator'] = session('admin_id');
            $data['building_id'] = session('building_id');

            $list = $this->select_find('goodss',['id'=>$data['goods_id']]);
            $res = $list['number'] - $data['num'];
            if( Db::name('consume')->insert($data)){
                //去减库存
                Db::table('goodss')->where('id',$data['goods_id'])->update(['number' => $res]);
                return $this->return_json('操作成功','100');
            }else{
                return $this->return_json('操作失败','0');
            }
        }
        $list =  Db::table('goodss')->where('building_id',session('building_id'))->paginate(10);
        return view('goods',['list' => $list,'room_id'=> input('room_id')]);
    }

    /*
     * 选择删除商品
     * */
    public function delgoods(){

        if(request()->isAjax()){
            $res = Db::table('consume')->where('id',input('id'))->find();
            $list = Db::table('goodss')->where('id',$res['goods_id'])->find();
            $data = [
                'id' => $res['goods_id'],
                'number' => $list['number'] + $res['num']
            ];
            if(  Db::table('consume')->delete(input('id'))){
                Db::table('goodss')->update($data);
                return $this->return_json('操作成功','100');
            }else{
                return $this->return_json('操作失败','0');
            }
        }
    }

    /*
     * 更换房间
     * */
    public function replace_room()
    {
        if(request()->isAjax()){

            if(empty(input('room_num'))){
                return $this->return_json('请选择需要更换的房间','0');
            }
            $result = Db::table('room')->where('id',input('room_id'))->find();
            $data['guest_name'] = $result['guest_name'];
            $data['activity_id'] = $result['activity_id'];
            $data['credentials'] = $result['credentials'];
            $data['guest_tel'] = $result['guest_tel'];
            $data['in_time'] = $result['in_time'];
            $data['move_time'] = $result['move_time'];
            $data['guest_sex'] = $result['guest_sex'];
            $data['guest_source'] = $result['guest_source'];
            $data['member_id'] = $result['member_id'];
            $data['payment_id'] = $result['payment_id'];
            $data['status'] = '2';
            if(Db::table('room')->where('id',input('replace_id'))->update($data)){
                $datas = [
                    'status' => 1,
                    'room_id' => 'no',
                    'guest_name' => '',
                    'activity_id' => '',
                    'credentials' => '',
                    'guest_sex' =>'',
                    'guest_source' =>'',
                    'guest_number' =>'',
                    'move_duration' =>'',
                    'move_time' => '',
                    'in_time' => ''
                ];
                Db::table('room')->where('id',input('room_id'))->update($datas);
                $res = Db::table('hotel_move_ren')->where('room_id',$result['room_num'])->update(['room_id'=>input('room_num')]);
                if($res){
                    Db::table('home_room_records')->insert([
                        'current_id'=> $result['room_num'],
                        'replace_id'=> input('room_num'),
                        'create_time'=> time(),
                        'building_id'=>session('building_id'),
                        'operator'=>session('admin_id')
                    ]);
                    return $this->return_json('更换成功','100');
                }else{
                    return $this->return_json('更换失败','0');
                }
            }
        }
        //查询当前房间的信息
        $room =  Db::table('room')
            ->alias('a')
            ->field('a.*,b.type_name,b.price,b.deposit,b.hour,c.building,d.storey,e.monday')
            ->join('layout b','a.type_id = b.id')
            ->join('building c','a.building_id = c.id')
            ->join('storey d','a.storey_id = d.id')
            ->join('week e','a.id = e.layout_id')
            ->where('a.id',input('id'))
            ->find();
        $map = [
            ['a.building_id','=',session('building_id')],
            ['a.status','=','1'],
        ];
        //查询所有房间信息
        $list =  Db::table('room')
            ->alias('a')
            ->field('a.*,b.type_name,b.price,b.deposit,b.hour,c.building,d.storey,e.monday')
            ->join('layout b','a.type_id = b.id')
            ->join('building c','a.building_id = c.id')
            ->join('storey d','a.storey_id = d.id')
            ->join('week e','a.id = e.layout_id')
            ->where($map)
            ->select();

        return view('replace_room',['room'=>$room,'list'=>$list]);
    }

}
