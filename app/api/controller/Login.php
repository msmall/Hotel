<?php
namespace app\api\controller;

use app\BaseController;
use think\facade\Db;

class Login extends BaseController
{
    public function index()
    {
        if(request()->isPost()){
            $data = input('param.');

            $list = Db::table('home_room_admin')->where('user_num',$data['user_num'])->find();

            if(!empty($list)){

                if($list['user_pwd'] == $data['user_pwd']){
					
					return json([
					    'msg' => '登录成功',
					    'code' => '200',
						'user_token'=> $this->user_token($list,$data['uid']),
                        'building_id'=>$list['building_id'],

					]);
                }else{
                    return self::return_json('密码错误','0');
                }
            }else{
                return self::return_json('账号不存在','0');
            }

        }

    }

    /*
     * 返回json数据
     * $msg（提示信息）
     * $code（状态码）
     * */
    public function return_json($msg,$code){
        return json([
            'msg' => $msg,
            'code' => $code
        ]);
    }
	
	/* 
	 * token生成
	 */
	public function user_token($data,$uid){
		$token = md5($data['user_num']).time().md5($data['building_id']).md5($data['id']);
        $token = base64_encode($token.rand(10000,9999999));
        Db::table('home_room_admin')->where('id',$data['id'])
            ->update([
                'token'=>md5($token),
                'token_time'=> time(),
                'uid'=>$uid
            ]);
		return md5($token);
	}
}
