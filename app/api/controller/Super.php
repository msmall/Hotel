<?php
namespace app\api\controller;

use app\BaseController;
use app\index\controller\Api;
use app\index\controller\Basics;
use think\facade\Db;

class Super extends Api
{


    /*
     * 返回json数据
     * $msg（提示信息）
     * $code（状态码）
     * */
    public function return_json($msg,$code){
        return json([
            'msg' => $msg,
            'code' => $code
        ]);
    }

    /*
     * token生成
     */
    public function user_token($data){
        $token = md5($data['tel']).md5($data['password']);
        $token = base64_encode($token.rand(10000,9999999));
        Db::table('app_member')->where('id',$data['id'])
            ->update([
                'token'=>md5($token)
            ]);
        return md5($token);
    }

    /*
     * 检查token
     * */
    public function check_token($token){
        $res = Db::table('app_member')->where('token',$token)->find();
        return $res;
    }

}
